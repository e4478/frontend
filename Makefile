all: build run

build:
	docker build . -t frontend:latest

run:
	docker run -p 8081:8081 frontend:latest
