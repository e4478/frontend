FROM node:16

# Create app directory
WORKDIR /usr/src/app

COPY . .

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

EXPOSE 8081
CMD [ "npm", "start" ]
