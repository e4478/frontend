import React, { Component } from "react";
import { Routes, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import AuthService from "./services/auth.service";
import Login from "./components/login.component";
import Register from "./components/register.component";
import Profile from "./components/profile.component";
import Elections from "./components/elections.component";
import CreateElection from "./components/create-election.component";
import ElectionDetails from "./components/election-details.component";
import ResetForm from "./components/reset-form.component";

class App extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);
    this.state = {
      showAdminBoard: false,
      currentUser: undefined,
    };
  }
  componentDidMount() {
    const user = AuthService.getCurrentUser();
    if (user) {
      this.setState({
        currentUser: user,
        showAdminBoard: user.role.includes("ROLE_ADMIN"),
      });
    }
  }
  logOut() {
    AuthService.logout();
  }
  render() {
    const { currentUser, showAdminBoard } = this.state;
    return (
      <div>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <Link to={"/elections"} className="navbar-brand">
            Evoting system
          </Link>
          <div className="navbar-nav mr-auto">
            {showAdminBoard && (
              <li className="nav-item">
                <Link to={"/create"} className="nav-link">
                  Stwórz głosowanie
                </Link>
              </li>
            )}
            {currentUser && (
              <li className="nav-item">
                <Link to={"/elections"} className="nav-link">
                  Wybory
                </Link>
              </li>
            )}
            {currentUser && (
              <li className="nav-item">
                <Link to={"/reset"} className="nav-link">
                  Zmień hasło
                </Link>
              </li>
            )}
          </div>
          {currentUser ? (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/profile"} className="nav-link">
                  {currentUser.username}
                </Link>
              </li>
              <li className="nav-item">
                <a href="/login" className="nav-link" onClick={this.logOut}>
                  Wyloguj
                </a>
              </li>
            </div>
          ) : (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/login"} className="nav-link">
                  Zaloguj
                </Link>
              </li>
              <li className="nav-item">
                <Link to={"/register"} className="nav-link">
                  Utwórz konto
                </Link>
              </li>
            </div>
          )}
        </nav>
        <div className="container mt-3">
          <Routes>
            <Route path="/login" element={<Login/>} />
            <Route path="/register" element={<Register/>} />
            <Route path="/profile" element={<Profile/>} />
            <Route path="/" element={<Elections/>} />
            <Route path="/elections" element={<Elections/>} />
            <Route path="/create" element={<CreateElection/>} />
            <Route path="/details" element={<ElectionDetails/>} />
            <Route path="/reset" element={<ResetForm/>} />
          </Routes>
        </div>
      </div>
    );
  }
}
export default App;