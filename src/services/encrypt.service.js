import { sha256 } from "js-sha256";
function encrypt(i_electionId, i_p, i_g, i_y, i_choices, i_choice) {

    function hash(values) {
        var originalString = "";
        for (var i = 0; i < values.length; i++) {
            originalString = originalString + values[i].toString();
        }
        console.log("originalString: " + originalString)
        var hash = sha256.create();
        hash.update(originalString);
        console.log(hash.hex());
        return BigInt("0x"+hash.hex());
    }

    function powmod(a, b, mod){
        if(b < 1){
          return BigInt(1);
        }
        const t = powmod(a, b >> BigInt(1), mod);
        if(b & BigInt(1)){
          return (a * t * t) % mod;
        }else{
          return (t * t) % mod;
        }
      }

    function modInverse(a, m) {
    let m0 = BigInt(m);
    let y = BigInt(0);
    let x = BigInt(1);
 
    if (m == 1)
        return 0;
 
    while (a > 1)
    {
        let q = BigInt(a / m);
        let t = m;
        m = a % m;
        a = t;
        t = y;
        y = x - q * y;
        x = t;
    }
    if (x < BigInt(0))
        x += m0;
 
    return BigInt(x);
}

    var p = BigInt(i_p);
    console.log("p: " + p);
    var g = BigInt(i_g);
    console.log("g: " + g);
    var y = BigInt(i_y);
    console.log("y: " + y);
    var choice = Number(i_choice);
    console.log("choice: " + choice);

    var primes = [];
    for (const [key, value] of Object.entries(i_choices)) {
        primes.push(BigInt(key));
    }

    var rx_array = new BigUint64Array(2);
    window.crypto.getRandomValues(rx_array);
    var r = BigInt(rx_array[0] >> BigInt(1));
    console.log("r: " + r);
    var x = BigInt(rx_array[1] >> BigInt(1));
    console.log("x: " + x);

    let ciphertext = {a: powmod(g,r,p), b: primes[choice]*((powmod(y,r,p))%p)};

    var u_array = new BigUint64Array(primes.length);
    var w_array = new BigUint64Array(primes.length);
    window.crypto.getRandomValues(u_array);
    window.crypto.getRandomValues(w_array);

    var u = [];
    var w = [];
    for (var i = 0; i < primes.length; i++) {
        if (i !== choice) {
            var new_u = u_array[i] >> BigInt(1);
            u.push(new_u);
            console.log("u" + i + ": " + new_u)
            var new_w = w_array[i] >> BigInt(1);
            w.push(new_w);
            console.log("w" + i + ": " + new_w)
        }
        else {
            u.push(BigInt(0));
            w.push(BigInt(0));
        }
    }

    var ap = [];
    var bp = [];
    for (var i = 0; i < primes.length; i++) {
        if (i !== choice) {
            var firstA = powmod(g,w[i],p);
            var secondA = powmod(ciphertext.a, u[i], p);
            var sj = primes[i];
            var firstB = powmod(y,w[i],p); 
            var secondB = powmod(modInverse(sj, p)*ciphertext.b, u[i], p);
            ap.push((firstA*secondA)%p);
            bp.push((firstB*secondB)%p);        
        }
        else {
            ap.push(null);
            bp.push(null);
        }
    }
    ap[choice] = powmod(g,x,p);
    bp[choice] = powmod(y,x,p)

    var values = [];
    values.push(...ap);
    values.push(...bp);

    var chall = hash(values);
    console.log("chall: " + chall.toString());
    var sumU = BigInt(0);
    for (var i = 0; i < u.length; i++) {
        sumU =  sumU + u[i];
    }
    console.log("sumU: " + sumU.toString());

    var uOption = chall - sumU;
    console.log("uOption: " + uOption.toString());
    u[choice] = uOption;
    w[choice] = x - r * uOption;

    for (var i = 0; i < u.length; i++) {
        u[i] = u[i].toString();
        w[i] = w[i].toString();
        ap[i] = ap[i].toString();
        bp[i] = bp[i].toString();
    }

    ciphertext.a = ciphertext.a.toString();
    ciphertext.b = ciphertext.b.toString();

    let body = {ap: ap, bp: bp, electionId: i_electionId, encryptedVote: ciphertext, u: u, w: w};
    return body;
}

export default encrypt;