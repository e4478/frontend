import React, { useState } from "react";
import { authHeader } from "../services/auth-header";
import { Navigate, useNavigate } from "react-router-dom";
function CreateElection() {
    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState();
    const [startDate, setStartDate] = useState("");
    const [endDate, setEndDate] = useState("");
    const [title, setTitle] = useState("");
    const [inputFields, setInputFields] = useState([{candidate: ""}])
    const navigate = useNavigate();

    const handleSubmit = (e) => {
        e.preventDefault();
        setLoading(true);
        const candidates = [];
        for (let i = 0; i < inputFields.length; i++) {
          if (inputFields[i].candidate === "") {
            setMessage("Wszystkie pola muszą być wypełnione");
            setLoading(false);
            return;
          }
          candidates.push(inputFields[i].candidate);
        }
        const token = authHeader();
        const body = {};
        if (startDate === "" || endDate === "" || title === "") {
          setMessage("Wszystkie pola muszą być wypełnione")
          setLoading(false);
          return;
        }
        body.startDate = startDate;
        body.endDate = endDate;
        body.title = title;
        body.candidates = candidates;
        console.log(JSON.stringify.body);
        const requestOptions = {
            method: 'POST',
            headers: { token, "Content-Type": "application/json" },
            body: JSON.stringify(body)
        };
        fetch("http://localhost:8080/election/add", requestOptions)
            .then(res => {
              if(!res.ok) throw new Error(res.status);
              else return res.JSON();
            })
            .then(
                (result) => {
                    setLoading(false);
                },
                (error) => {
                    setLoading(false);
                    setMessage(message);
                }
            )
            .catch((error) => {
              setMessage(error.toString());
            });
            navigate('/elections');
            window.location.reload(false);
    }

    const handleFormChange = (index, event) => {
      let data = [...inputFields];
      data[index][event.target.name] = event.target.value;
      setInputFields(data);
    }

    const addFields = (e) => {
      e.preventDefault();
      let newField = {candidate: ""};
      setInputFields([...inputFields, newField]);
    }

    const removeFields = (e) => {
      e.preventDefault();
      let data = [...inputFields];
      let popped = data.pop();
      setInputFields(data);
    }

    return (
        <div className="col-md-12">
        <div className="card card-container">
          <form>
            <div className="form-group">
              <label htmlFor="title">Nazwa</label>
              <input type="text" className="form-control" name="title" value={title} onChange={e => setTitle(e.target.value)} />
            </div>
            <div className="form-group">
              <label htmlFor="startDate">Początek głosowania</label>
              <input
                type="datetime-local"
                className="form-control"
                name="startDate"
                value={startDate}
                onChange={e => setStartDate(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label htmlFor="endDate">Koniec głosowania</label>
              <input
                type="datetime-local"
                className="form-control"
                name="endDate"
                value={endDate}
                onChange={e => setEndDate(e.target.value)}
              />
            </div>
            {inputFields.map((input, index) => {
              return (
                <div key={index} className="form-group">
                  <label htmlFor="candidate">{index + 1}. kandydat</label>
                  <input name="candidate"
                    type="text"
                    value={input.candidate}
                    onChange={event => handleFormChange(index, event)}
                    required
                  />
                </div>
              )
            })}
            <div className="form-group">
              <button
                className="btn btn-primary btn-block"
                disabled={loading}
                onClick={handleSubmit}
              >
                {loading && (
                  <span className="spinner-border spinner-border-sm"></span>
                )}
                <span>Utwórz</span>
              </button>
            </div>
            <div className="form-group">
              <button
                className="btn btn-secondary btn-block me-1"
                disabled={loading}
                onClick={e => addFields(e)}
                >
                  <span>+ dodaj kandydata</span>
                </button>
              <button
                className="btn btn-secondary btn-block"
                disabled={loading}
                onClick={e => removeFields(e)}
              >
                <span>- usuń kandydata</span>
              </button>
            </div>
            {message && (
              <div className="form-group">
                <div className="alert alert-danger" role="alert">
                  {message}
                </div>
              </div>
            )}
          </form>
        </div>
      </div>
    )
}
export default CreateElection;