import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { authHeader } from "../services/auth-header";

function ResetForm() {
    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState();
    const [oldPassword, setOldPassword] = useState("");
    const [newPassword, setNewPassword] = useState("");
    const [repeatPassword, setRepeatPassword] = useState("");
    const navigate = useNavigate();

    const handleSubmit = (e) => {
        e.preventDefault();
        setLoading(true);
        if (oldPassword === "" || newPassword === "" || repeatPassword === "") {
            setMessage("Wszystkie pola muszą być wypełnione");
            setLoading(false);
            return;
        }
        if (newPassword !== repeatPassword) {
            setMessage("Hasła muszą być identyczne");
            setLoading(false);
            return;
        }
        const token = authHeader();
        const body = {oldPassword: oldPassword, newPassword: newPassword}
        const requestOptions = {
            method: "POST",
            headers: { "Token": token, "Content-Type": "application/json" },
            body: JSON.stringify(body)
        };
        fetch("http://localhost:8080/changePassword", requestOptions)
            .then(res => {
                setLoading(false);
                if(!res.ok) throw new Error(res.status)
                else {
                    navigate('/');
                    window.location.reload(false);
                    return res;
                }
            })
            .catch(err => {
                setLoading(false);
                setMessage(err.toString());
            })

    }

    return (
        <div className="col md-12">
            <div className="card card-container">
                <form>
                    <div className="form-group">
                        <label htmlFor="old">Stare hasło</label>
                        <input
                            type="password"
                            className="form-control"
                            name="old"
                            value={oldPassword}
                            onChange={e => setOldPassword(e.target.value)}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="new">Nowe hasło</label>
                        <input
                            type="password"
                            className="form-control"
                            name="new"
                            value={newPassword}
                            onChange={e => setNewPassword(e.target.value)}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="repeat">Powtórz nowe hasło</label>
                        <input
                            type="password"
                            className="form-control"
                            name="repeat"
                            value={repeatPassword}
                            onChange={e => setRepeatPassword(e.target.value)}
                        />
                    </div>
                    <div className="form-group">
                        <button 
                            className="btn btn-primary btn-lock"
                            disabled={loading}
                            onClick={handleSubmit}
                        >
                            {loading && (
                                <span className="spinner-board spinner-board-sm"></span>
                            )}
                            <span>Zmień hasło</span>
                        </button>
                    </div>
                    {message && (
                        <div className="form-group">
                            <div className="alert alert-danger" role="alert">
                                {message}
                            </div>
                        </div>
                    )}
                </form>
            </div>
        </div>
    )
}

export default ResetForm;