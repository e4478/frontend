import { useNavigate } from "react-router-dom";

function Election(props) {
    
    function formatDate(date) {
        var year = date.substring(0, 4);
        var month = date.substring(5, 7);
        var day = date.substring(8, 10);
        var hour = date.substring(11, 13);
        var minute = date.substring(14, 16);
        return day + '-' + month + '-' + year + ' ' + hour + ':' + minute;
    }

    const today = Date.now() + 7200000; //change to Warsaw time +2 hours
    const startTimestamp = Date.parse(props.startDate);
    const endTimestamp = Date.parse(props.endDate);
    const startDate = formatDate(props.startDate);
    const endDate = formatDate(props.endDate);
    var status;

    if (startTimestamp < today && endTimestamp > today) {
        status = 0;
    }

    else if (startTimestamp > today) {
        status = 1;
    }

    else if (endTimestamp < today) {
        status = -1;
    }

    const navigate = useNavigate();

    const navigateToDetails = () => {
        navigate('/details', {state:{id:props.id, status: status, startDate: startDate, endDate: endDate,
            title: props.title, choices: props.choices, isVoted: props.isVoted, p: props.p, g: props.g, y: props.y,
            startTimestamp: startTimestamp, endTimestamp: endTimestamp}})
    }

    return (
        <div className="card mb-3" style={{width: "500px"}}>
            <div className="card-body">
            <h5 className="card-title">{props.title}</h5>
            <p className="card-title">Stan: 
                { status === 0 && !props.isVoted && " trwa" }
                { status === 0 && props.isVoted && " oddano głos" }
                { status === 1 && " oczekiwanie" }
                { status === -1 &&  " zakończono" }
            </p>
            <p className="card-text">Poczatek: {startDate}</p>
            <p className="card-text">Koniec: {endDate}</p>
            <a onClick={() =>{navigateToDetails()}} className="card-link">Zobacz szczegóły</a>
            </div>
        </div>
    )
}
export default Election;