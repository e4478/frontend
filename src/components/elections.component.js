import React, { Component } from 'react';
import { authHeader } from '../services/auth-header';
import Election from './election.component';
export default class Elections extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: [],
            isLoggedIn: false
        };
    }

    componentDidMount() {
        const token = authHeader();
        if (token) {
            this.setState({isLoggedIn: true})
        
        const requestOptions = {
            method: 'GET',
            headers: { token },
        };
        fetch("http://localhost:8080/election/elections", requestOptions)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
        }
    }

    render() {
        const { error, isLoaded, items, isLoggedIn } = this.state;
        if (error) {
            return <div>Blad: {error.message}</div>;
        }
        if (!isLoggedIn) {
            return <div>Zaloguj sie by zobaczyc dostepne wybory</div>
        }
        return (
            <ul>
                {items.map(item => (
                    <Election
                        key = {item.id}
                        id = {item.id}
                        title = {item.title}
                        startDate = {item.startDate}
                        endDate = {item.endDate}
                        choices = {item.choices}
                        isVoted = {item.isVoted}
                        p = {item.p}
                        g = {item.g}
                        y = {item.y}
                    />
                ))}
            </ul>
        );
    }
}