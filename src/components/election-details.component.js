import { Navigate, useLocation, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { authHeader } from "../services/auth-header";
import encrypt from "../services/encrypt.service";
function ElectionDetails() {

    const location = useLocation();
    const [choice, setChoice] = useState();
    const [results, setResults] = useState();
    const [error, setError] = useState("");
    const [isLoaded, setIsLoaded] = useState(false);
    const [loading, setLoading] = useState(false);
    const [status, setStatus] = useState();
    const navigate = useNavigate();

    const handleChange = (e) => {
        setChoice(e.target.value);
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        setLoading(true);
        const body = encrypt(location.state.id, location.state.p, location.state.g, location.state.y, location.state.choices, choice);
        const token = authHeader();
        const requestOptions = {
            method: 'POST',
            headers: { "Token": token, "Content-Type": "application/json" },
            body: JSON.stringify(body)
        };
        fetch("http://localhost:8080/election/vote", requestOptions)
            .then(res => {
                if (!res.ok) throw new Error(res.status);
                else return res.json
            })
            .then(
                (result) => {
                    setLoading(false);
                    console.log("Success");
                    navigate('/elections');
                    window.location.reload(false);
                },
                (error) => {
                    console.log("Error :(");
                    setLoading(false);
                    // window.location.reload(false);
                }
            )
    }

    useEffect(() => {
        const today = Date.now() + 7200000; //change to Warsaw time +2 hours
        if (location.state.startTimestamp < today && location.state.endTimestamp > today) {
            setStatus(0);
        }
    
        else if (location.state.startTimestamp > today) {
            setStatus(1);
        }
    
        else if (location.state.endTimestamp < today) {
            setStatus(-1);
        }
        if (location.state.endTimestamp < today) {
            console.log(location.state.id);
            const token = authHeader();
            const requestOptions = {
                method: 'GET',
                headers: { token }
            };
            fetch("http://localhost:8080/election/election?electionId=" + location.state.id, requestOptions)
                .then(res => res.json())
                .then(
                    (result) => {
                        setResults(result);
                        setIsLoaded(true);
                    },
                    (error) => {
                        setError(error);
                    }
                )
        }
    }, []);

    function renderChoices() {
        var choices = [];
        var index = 0;
        for (const [key, value] of Object.entries(location.state.choices)) {
            choices.push(
                <div className="form-check" key={key}>
                    <input className="form-check-input" type="radio" name="choices" id={value} value={index} />
                    <label className="form-check-label" htmlFor={value}>
                        {value}
                    </label>
                </div>
            )
            index++;
        }
        return <div onChange = {handleChange}>{choices}</div>;
    }

    function renderResults() {
        var results_array = [];
        for (const [key, value] of Object.entries(results)) {
            results_array.push(
                <li className="list-group-item d-flex justify-content-between allign-items-center" key={key}>
                    {key}: {value}
                    <span className="badge badge-primary badge-pill">{value}</span>
                </li>
            )
        }
        return results_array;
    }

    return (
        <div className="col md-12">
            <div className="card card-container">
                <h1>{location.state.title}</h1>
                {location.state.isVoted
                    &&<h3>Oddałeś już swój głos w tym głosowaniu</h3>}
                {status === -1
                    &&<h3>Głosowanie zakończono {location.state.endDate}</h3>
                }
                {status === -1
                    &&<h3>Wyniki:</h3>}
                {status === -1
                    && isLoaded
                    && (
                        <ul className="list-group">
                            {renderResults()}
                        </ul>
                    )
                }
                {status === 1
                    && <h3>Głosowanie rozpocznie się {location.state.startDate}</h3>}
                {status === 0
                    &&<h3>Głosowanie kończy się {location.state.endDate}</h3>}
                <form style={{visibility: status === -1 ? 'hidden' : 'visible'}}>
                    {renderChoices()}
                    <div className="form-group">
                    <button
                        className="btn btn-primary btn-block"
                        onClick={handleSubmit}
                        disabled={status !==0 || location.state.isVoted || loading}
                    >
                        {loading &&(
                            <span className="spinner-board spinner-board-sm"></span>
                        )}
                        <span>Zagłosuj</span>
                    </button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default ElectionDetails;